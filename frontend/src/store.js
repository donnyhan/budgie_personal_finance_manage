import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { getSecret, getUniqueSecret, isDevMode, isMockAndDummyMode, logging } from 'utils/config';
import rootReducer from './reducer/index'
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { encryptTransform } from 'redux-persist-transform-encrypt';
import { getStoreKeyConfig } from 'utils';

const persistConfig = {
    key: getStoreKeyConfig(),
    whitelist: ['login', 'menu', 'widget'],
    storage,
    transforms: [
        encryptTransform({
          secretKey: getUniqueSecret(),
          onError: function (err) {
            logging('err', err)
            localStorage.removeItem('persist:budgie');
            window.location.reload(true)
          },
        }),
    ]
}

const composeEnhancer = (isDevMode() && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(
    persistedReducer,
    composeEnhancer(applyMiddleware(thunk, logger))
)

const  persistor = persistStore(store);

export {store, persistor}