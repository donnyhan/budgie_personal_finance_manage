import React, { useCallback, useEffect, useState } from 'react'
import { Dropdown, Layout, Menu, Avatar, Modal, message, Badge, Button, Divider } from 'antd';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import { handleGlobalErrorFunc, useHasChanged, useWindowSize } from 'utils';
// import PropTypes from 'prop-types'
import './index.scss'
import {
    UnlockOutlined,
    LogoutOutlined,
    DownOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    BellOutlined,
    SettingOutlined,
    MoneyCollectOutlined,
    WechatOutlined,
    BulbOutlined,
    FlagOutlined,
    LineChartOutlined,
    ProjectOutlined
} from '@ant-design/icons'
import { isDummyMode, logging } from 'utils/config';
import Emitter from 'utils/emitter';
import { setLoginInfo } from 'action/login';
import { setRoutePath } from 'action/info';
import Profile from './profile';
import _ from 'lodash';
const { SubMenu } = Menu;
const { Header } = Layout;

const HeaderComponent = () =>{
    const history = useHistory();
    const dispatch = useDispatch();
    const loginState  = useSelector(state => state.login)
    const infoState  = useSelector(state => state.info)
    const loginInfo = _.get(loginState, 'loginInfo', {})
    const selectedPath = _.get(infoState, 'routePath', 'home');
    const location = useLocation();
    const pathNameHasChanged = useHasChanged(location.pathname)
    const [isShowProfile, setShowProfile] = useState(false);
    const size = useWindowSize();
    useEffect(() => {
        if (location.pathname && pathNameHasChanged) {
            let pathKey = (location.pathname).replace('/index/', '');
            dispatch(setRoutePath(pathKey))
        }
    }, [location])

    const menuChange = (item) =>{
        switch(item.key){
            case "logout":
                Modal.confirm({
                    title:'Are you sure want to logout?',
                    okText:'OK',
                    okType:'danger',
                    cancelText:'Cancel',
                    onOk(){
                        logoutHandle();
                    }
                });
                break;
            case "profile":
                setShowProfile(true);
                break;
            default: 
                return;

        }
    }
    const logoutHandle = async () => {
        dispatch(setLoginInfo(null))
        setTimeout(() => {
            message.success('Successfully Logout');
            history.push('/login')
        }, 200);
    }

    const redirectTo = (path) => {
        history.push(`/index/${path}`)
    }

    const hideProfile = () =>{
        setShowProfile(false)
    }

    const email = _.get(loginInfo, 'email', '')
    return (
        <Header className='layoutHeader'>
            <div className="logo white-text">
                Budgie
            </div>
            <div className='userInfo'>
                <div className="right-item">
                    {
                        (size && size.width >= 1024) ?
                        <>
                            <div className="menu-things margin-horizontal-10">
                                <Button type="link" className={`white-text ${(selectedPath === 'home' ? 'bold-text' : null)}`} shape="circle" onClick={() => redirectTo('home')}><LineChartOutlined /> Overview</Button>
                            </div>
                            <div className="menu-things margin-horizontal-10">
                                <Button type="link" className={`white-text ${(selectedPath === 'finance' ? 'bold-text' : null)}`} shape="circle" onClick={() => redirectTo('finance')}><ProjectOutlined />Finance</Button>
                            </div>
                            <Divider type="vertical" className="header-white-divider"/>
                        </>
                        :
                        <>
                            <div className="menu-things">
                                <Button type="link" className={`white-text ${(selectedPath === 'home' ? 'bold-text' : null)}`} shape="circle" onClick={() => redirectTo('home')}><LineChartOutlined /></Button>
                            </div>
                            <div className="menu-things margin-horizontal-10">
                                <Button type="link" className={`white-text ${(selectedPath === 'finance' ? 'bold-text' : null)}`} shape="circle" onClick={() => redirectTo('finance')}><ProjectOutlined /></Button>
                            </div>
                            <Divider type="vertical" className="header-white-divider"/>
                        </>
                    }
                    <Dropdown className='drop' trigger={['click']} overlay={
                        <Menu onClick={menuChange}>
                            <Menu.Item key='profile'><UnlockOutlined />Profile</Menu.Item>
                            <Menu.Item key='logout'><LogoutOutlined/>Logout</Menu.Item>
                        </Menu>
                    }>
                        <div>
                            <Avatar className="green-avatar">{email ? email.substring(0, 1).toUpperCase() : ''}</Avatar>
                            {
                                (size && size.width >= 1024) ?
                                <span className='user white-text'>{email}</span>
                                :
                                null
                            }
                            <span className='down-outline-icon'>
                                <DownOutlined />
                            </span>
                        </div>
                    </Dropdown>
                </div>
            </div>
            {
                isShowProfile &&
                <Profile 
                    onCancel={hideProfile}
                ></Profile>
            }
        </Header>
    )
}
HeaderComponent.propTypes = {
}
export default HeaderComponent;