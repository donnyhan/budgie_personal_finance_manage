import React, {useState, useEffect} from 'react';
import { Form, Input, message, Modal } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons'
import PropTypes from 'prop-types';
import { useHistory, userHistory } from  'react-router-dom';
import {
    changePasswordAPI
} from 'api/login'
import { useDispatch, useSelector } from 'react-redux';
import { setLoginInfo } from 'action/login';
import { handleGlobalErrorFunc } from 'utils';
import _ from 'lodash';
const Item = Form.Item;
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
let secondsToGo = 3;
let modal = null;
let timer = null;
const Profile = (props) => {
    const {onCancel} = props;
    const [form] = Form.useForm();
    const history = useHistory();
    const dispatch = useDispatch();
    const loginState  = useSelector(state => state.login)
    const loginInfo = _.get(loginState, 'loginInfo', {})
    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 }
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 }
        }
    };
    const [newPsw, setNewPsw] = useState('');

    const newPswChange = (e)  =>{
        setNewPsw(e.target.value);
    }

    const onOk = () => {
        form.validateFields().then( async (values)=>{
            const params = {
                oldPassword: values.oldPassword,
                newPassword: values.newPassword,
                confirmPassword: values.confirmPassword
            }
            try {
                const res = await changePasswordAPI(params);
                countDown()
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }).catch((err)=>{

        })
    }

    const countDown = () => {
        modal = Modal.success({
            title: 'Success',
            content: `You will redirected to login screen immediately.`,
            footer:null
        });
        timer = setInterval(() => {
            secondsToGo -= 1;
            modal.update({
                content: `You will redirected to login screen in ${secondsToGo} second(s)`,
            });
        }, 1000);
        setTimeout(() => {
            dispatch(setLoginInfo(null))
            message.success('Successfully Logout.');
            history.push('/login')
        }, secondsToGo * 1000);
    }
    return (
        <Modal
            title="Profile"
            visible={true}
            okText="OK"
            cancelText="Cancel"
            onOk={onOk}
            onCancel={onCancel}
            width={'70%'}
            closable={false}
        >
            <Form {...layout} form={form} name="profile">
                <Item
                    {...formItemLayout}
                    label="E-mail Address"
                >
                    <div className="bold-text">
                        {_.get(loginInfo, 'email', '')}
                    </div>
                </Item>
                <Item
                    {...formItemLayout}
                    name="oldPassword"
                    label="Current Password"
                    rules={
                        [
                            {
                                required: true, message: "Current Password is required"
                            }
                        ]
                    }
                >
                    <Input.Password placeholder="Enter Current Password" iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}></Input.Password>
                </Item>
                <Item
                    {...formItemLayout}
                    name="newPassword"
                    label="New Password"
                    rules={
                        [
                            {
                                required: true, message: "New Password is required"
                            }
                        ]
                    }
                >
                    <Input.Password placeholder="Enter Current Password" onChange={newPswChange} iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}></Input.Password>
                </Item>
                <Item
                    {...formItemLayout}
                    name="confirmPassword"
                    label="Confirm New Password"
                    rules={
                        [
                            {
                                required: true, message: "Confirm Password is required"
                            },
                            {
                                validator:(rule, value, callback) => {
                                    try {
                                        if(newPsw !== value){
                                            throw new Error('Password is mismatch');
                                        }else{
                                            callback();
                                        }
                                    } catch (err) {
                                        callback(err);
                                    }
                                }
                            }
                        ]
                    }
                >
                    <Input.Password placeholder="Enter Confirm New Password" iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}></Input.Password>
                </Item>
            </Form>
        </Modal>
    )
}
Profile.propTypes = {
    onCancel: PropTypes.func.isRequired
}
export default Profile;