import React from 'react';
import {  Layout } from 'antd';
import { getCurrentYear } from 'utils/config';
const {  Footer } = Layout;

const FooterComponent = () => {
    const currentYear = getCurrentYear();
    return (
        <Footer>
            Copyright {currentYear}
        </Footer>
    )
}

export default FooterComponent;