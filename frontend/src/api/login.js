import http from 'utils/http'

export const login = async (params) => {
    return await http.post('/api/user/login', params, {form: false})
}

export const register = async (params) => {
    return await http.post('/api/user/register', params, {form: false})
}

export const getCurrentProfile = async (params) => {
    return await http.get('/api/user/profile', {params: params})
};

export const changePasswordAPI = async (params) => {
    return await http.post('/api/user/changePassword', params, {form: false})
}