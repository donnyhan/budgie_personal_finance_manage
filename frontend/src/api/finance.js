import http from 'utils/http'

export const getAccountsListAPI = async (params) => {
    return await http.post('/api/accounts/list', params, {form: false})
};

export const deleteAccountAPI = async (params) => {
    return await http.post('/api/accounts/delete', params, {form: false})
}

export const createAccountsAPI = async (params) => {
    return await http.post('/api/accounts/create', params, {form: false})
}

export const editAccountsAPI = async (params) => {
    return await http.post('/api/accounts/edit', params, {form: false})
}

export const getTransactionsListAPI = async (params) => {
    return await http.post('/api/transactions/list', params, {form: false})
};

export const deleteTransactionsAPI = async (params) => {
    return await http.post('/api/transactions/delete', params, {form: false})
}

export const createTransactionsAPI = async (params) => {
    return await http.post('/api/transactions/create', params, {form: false})
}

export const editTransactionsAPI = async (params) => {
    return await http.post('/api/transactions/edit', params, {form: false})
}

export const getTopExpensesAPI = async (params) => {
    return await http.post('/api/transactions/topExpenses', params, {form: false})
}

export const getTransactionsChartDataAPI = async (params) => {
    return await http.post('/api/transactions/chartData', params, {form: false})
}