export const getMenuBySessionUserDummy = () => {
    return {
        "status": "200",
        "data": [
            {
                "key": "home",
                "title": "Beranda",
                "icon": "home",
                "permission": {
                    "create": false,
                    "read": true,
                    "update": false
                }
            },
            {
                "key": "settings",
                "title": "Pengaturan",
                "icon": "settings",
                "permission": {
                    "create": false,
                    "read": true,
                    "update": false
                },
                "child": [
                    {
                        "key": "payment",
                        "title": "Pembayaran",
                        "icon": "settings",
                        "permission": {
                            "create": true,
                            "read": true,
                            "update": true
                        },
                        "child": [
                            {
                                "key": "wallet",
                                "title": "Atur Wallet",
                                "icon": "wallet",
                                "permission": {
                                    "create": true,
                                    "read": true,
                                    "update": true
                                }
                            },
                            {
                                "key": "credit",
                                "title": "Kredit Anda",
                                "icon": "swap",
                                "permission": {
                                    "create": true,
                                    "read": true,
                                    "update": true
                                }
                            },
                        ]
                    }
                ]
            }
        ],
        "message": "Success"
    }
}