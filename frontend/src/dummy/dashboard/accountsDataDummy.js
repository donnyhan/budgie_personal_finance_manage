export const accountsDataDummy = () => {
    return {
        "data": [
            {
                "id": 1,
                "label": "Cash"
            },
            {
                "id": 2,
                "label": "Card",
            },
            {
                "id": 3,
                "label": "Savings",
            },
            {
                "id": 4,
                "label": "Top Up / Prepaid",
            },
            {
                "id": 5,
                "label": "Loan",
            },
        ]
    }
}