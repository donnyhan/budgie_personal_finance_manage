export const chartDataDummy = () => {
  return { 
    "data": [
      { "sum": "-400000", "category": "Expense", "date": "2021-05-12T08:20:58.000Z" }, 
      { "sum": "1000000", "category": "Income", "date": "2021-05-12T08:20:58.000Z" }, 
      { "sum": "20000200", "category": "Income", "date": "2021-05-16T00:46:38.000Z" }
    ] 
  }
}