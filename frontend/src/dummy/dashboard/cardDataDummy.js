export const cardDataDummy = () => {
    return {
        "data": [
            {
                "name": "Shopping Cost",
                "amount": 200000
            },
            {
                "name": "Clothes & Shoes",
                "amount": 180000,
            },
            {
                "name": "Canon Camera",
                "amount": 100000,
            },
            {
                "name": "Home Loan",
                "amount": 8000,
            },
            {
                "name": "Birthday Gift",
                "amount": 1000,
            }
        ]
    }
}