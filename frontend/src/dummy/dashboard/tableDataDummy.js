export const tableDataDummy = () => {
    return {
        "data": {
            "list": [
                {
                    "date": "2021-05-11T10:54:12.000Z",
                    "amount": 2500,
                    "name": "Shopping Cost"
                },
                {
                    "date": "2021-05-11T10:54:12.000Z",
                    "amount": 2500,
                    "name": "Clothes & Shoes"
                },
                {
                    "date": "2021-05-11T10:54:12.000Z",
                    "amount": 2500,
                    "name": "Shopping Cost"
                },
                {
                    "date": "2021-05-11T10:54:12.000Z",
                    "amount": 2500,
                    "name": "Shopping Cost"
                },
                {
                    "date": "2021-05-11T10:54:12.000Z",
                    "amount": 2500,
                    "name": "Shopping Cost"
                }
            ],
            "currentPage": 1,
            "pageSize": 10,
            "totalItem": 2
        }
    }
}