import { SET_ACCOUNTS_LIST } from "constants/types"
export const setAccountList = (params) => {
    return {
        type: SET_ACCOUNTS_LIST,
        params: params
    }
}