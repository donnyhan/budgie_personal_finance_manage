import { SET_ROUTE_PATH } from "constants/types"
export const setRoutePath = (params) => {
    return {
        type: SET_ROUTE_PATH,
        params: params
    }
}