import { LOGIN } from "constants/types"
export const setLoginInfo = (params) => {
    return {
        type: LOGIN,
        params: params
    }
}