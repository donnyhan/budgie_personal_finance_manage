import { setLoginInfo } from 'action/login';
import { message } from 'antd';
import axios from 'axios';
import { CONST_MESSAGE_SESSION_EXPIRED } from 'constants/constant';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import { store } from 'store';
import { getCookie } from '.';
import {  baseURL, logging } from './config'

const instance = axios.create({
    baseURL: baseURL(),
    // timeout: 1000,
    headers: {
      'Accept': 'application/json'
    }
});

instance.interceptors.request.use(
  (config) => {
    logging('config', config)
    const loginState = store.getState();
    let token = _.get(loginState, 'login.loginInfo.token', null);
    let url = config.url;
    let worldtimeAPIFlag = (url.indexOf('/worldtime') !== -1)
    if (url.indexOf('api/downloadfile/') !== -1) {
      config.responseType = 'arraybuffer';
    }
    if (token && url !== "/api/login") {
      config.headers.Authorization =  `bearer ${token}`;
    }
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

instance.interceptors.response.use(function (response) {
    logging('response', response)
    if (response.config.url.indexOf('/api/downloadfile') !== -1 || response.config.url.indexOf('/api/uploadfile') !== -1) {
      return response;
    } else {
      return response.data;
    }
  }, function (error) {
    logging('error', error)
    if(error.response.status === 401) {
      store.dispatch(setLoginInfo({
        name: null, 
        email: null, 
        userId: null
      }));
      setTimeout(() => {
        message.error(CONST_MESSAGE_SESSION_EXPIRED)
      }, 500)
      window.location.href = '/login';
    } else {
      logging(error)
    }
    return Promise.reject(error);
});

export default instance;