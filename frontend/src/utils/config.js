import { message } from 'antd';
import * as moment from 'moment';
import * as momentTz from 'moment-timezone';
import { formatMoney } from 'utils';
import * as uuid from "uuid";
momentTz.tz.setDefault('Asia/Jakarta');

message.config({
    top: 100,
    duration: 2,
    maxCount: 1,
    rtl: false
});

export const baseURL = () =>{
    // return (process.env.REACT_APP_HOST_URL) ? process.env.REACT_APP_HOST_URL : ''
    return ''
}
export const logging = (...arg) => {
    if (isDevMode()) {
        console.log(...arg);
    }
}
export const isDevMode = () => {
    return (process.env.REACT_APP_NODE_ENV === 'development' || process.env.REACT_APP_NODE_ENV === 'mock' || process.env.REACT_APP_NODE_ENV === 'dummy')
}
export const isMockAndDummyMode = () => {
    return (process.env.REACT_APP_NODE_ENV === 'mock' || process.env.REACT_APP_NODE_ENV === 'dummy')
}
export const isDummyMode = () => {
    return process.env.REACT_APP_NODE_ENV === 'dummy';
}
export const getSecret = () => {
    return process.env.REACT_APP_KEY;
}
export const getUniqueSecret = () => {
    let secret = localStorage.getItem('uuid') || false;
    if (!secret) {
        secret = uuid.v4();
        localStorage.setItem('uuid', secret);
    }
    return secret;
}

export const getCurrentYear = () => {
    return moment().format('YYYY');
}

export const lineChartConfig = (dt) => {
    return {
        data: dt,
        xField: "date",
        yField: "formattedSum",
        seriesField: "category",
        yAxis: {
          label: {
            formatter: function formatter(v) {
              return "".concat(v).replace(/\d{1,3}(?=(\d{3})+$)/g, function (s) {
                return "".concat(s, ",");
              });
            }
          }
        },
        interactions: [{ type: "brush" }]
    }
}