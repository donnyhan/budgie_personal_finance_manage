import { logging } from "./config";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import {
    CONST_MESSAGE_SESSION_EXPIRED
} from "constants/constant";
import { message, Modal } from "antd";
import { Redirect, Route, useHistory } from "react-router-dom";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect, useRef, useState } from "react";
const defaultMaskOptions = {
    prefix: "",
    suffix: "",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ".",
    allowDecimal: true,
    decimalSymbol: ",",
    decimalLimit: 2,
    integerLimit: 1000,
    allowNegative: true,
    allowLeadingZeroes: false,
};

export const currencyMask = createNumberMask(defaultMaskOptions);

export function formatMoney(amount, decimalCount = defaultMaskOptions.decimalLimit, decimal = defaultMaskOptions.decimalSymbol, thousands = defaultMaskOptions.thousandsSeparatorSymbol) {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        logging(e)
        return null
    }
};
export function unformatMoney(val = '') {
    return Number(val.toString().replace(/\ /g, '').replace(/\./g, '').replace(/\,/g, '.'))
}

export const isUserLoggedIn = (loginInfo) => {
    if (loginInfo && loginInfo.token) {
        return true;
    } else {
        return false;
    }
}

export const getStoreKeyConfig = () => {
    return `BUDGIE`;
}

export const PrivateRoute = ({ component: Component, props, ...rest }) => {
    const history = useHistory()
    const dispatch = useDispatch();
    const login = useSelector(state => state.login)
    const loginInfo = _.get(login, 'loginInfo', {})
    return (
        <Route
            {...rest}
            render={() => isUserLoggedIn(loginInfo)
                ? <Component {...props} />
                : <Redirect to={{ pathname: '/login', state: { from: (history && history.location) ? history.location : null } }} />}
        />
    )
}
export const HomeRedirectorRoute = ({ component: Component, props, ...rest }) => {
    const history = useHistory()
    const dispatch = useDispatch();
    const login = useSelector(state => state.login)
    const loginInfo = _.get(login, 'loginInfo', {})

    return (
        <Route
            {...rest}
            render={() => isUserLoggedIn(loginInfo)
                ? <Redirect to={{ pathname: '/index/home', state: { from: (history && history.location) ? history.location : null } }} />
                : <Component {...props} />}
        />
    )
}

export const useWindowSize = () => {
    const [windowSize, setWindowSize] = useState({
        width: undefined,
        height: undefined,
    });

    useEffect(() => {
        // Handler to call on window resize
        function handleResize() {
            // Set window width/height to state
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }

        window.addEventListener("resize", handleResize);

        handleResize();

        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return windowSize;
}
export const useIsMounted = () => {
    const currentRef = useRef(true);
    const isMounted = useCallback(() => currentRef.current, []);

    useEffect(() => {
        return () => void (currentRef.current = false);
    }, []);

    return isMounted;
}

export const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

export const useHasChanged = (val) => {
    const prevVal = usePrevious(val)
    return prevVal !== val
}

export const handleGlobalErrorFunc = (error) => {
    logging('error', error)
    let defaultMessage = 'Terjadi Kesalahan saat melalukan request ke server';
    let defaultTitle = 'Terjadi Kesalahan'
    let resData = _.get(error, 'response.data', null)
    let errorMessage = _.get(error, 'response.data.message', defaultMessage);
    (errorMessage !== '') ? errorMessage = errorMessage : errorMessage = defaultMessage;
    let errorTitle = _.get(error, 'response.data.title', defaultTitle);
    let statusCode = _.get(error, 'response.status', null)
    let statusMessage = (statusCode) ? `(${statusCode})` : ''
    if (error.response && resData !== null && resData.type === 'modal') {
        Modal.error({
            title: errorTitle,
            content: errorMessage,
        });
    } else if (error.response && resData !== null) {
        message.error(`${errorMessage} ${statusMessage}`)
    } else if (error.request) {
        message.error(defaultMessage)
        logging('Error', error.request);
    } else {
        message.error(defaultMessage)
        logging('Error', error.message);
    }
}

export const formatNumberWithComma = (rawNumber) => {
    return rawNumber.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}