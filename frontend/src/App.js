import './App.scss';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Login from './views/Login';
import Register from './views/Register';
import Layout from './views/Layout'
import NoMatch from './views/NoMatch';
import { useEffect } from 'react';
import ReactDOM from 'react-dom'
import { HomeRedirectorRoute } from 'utils';
import Forbidden from 'views/Forbidden';
function App() {
  useEffect(() => {
    let element = document.getElementById('loader')
    ReactDOM.findDOMNode(element).classList.add("display-none");
  }, [])
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <HomeRedirectorRoute path='/login' component={Login}></HomeRedirectorRoute>
          <Route path='/register'>
            <Register></Register>
          </Route>
          <Route path='/index'>
            <Layout></Layout>
          </Route>
          <Redirect from="/*" to="/login" />
          <Route path='/*'>
            <NoMatch></NoMatch>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
