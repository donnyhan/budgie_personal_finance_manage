import { SET_ROUTE_PATH } from "constants/types";
const initState = {
    routePath: 'home'
}

export default function info(state = initState, action){
    switch(action.type){
        case SET_ROUTE_PATH:
            return {...state, routePath: action.params};
        default:
            return state;
    }
}