import { LOGIN } from "../constants/types";
const initState = {
    loginInfo:{
        token: null,
        id: null,
        email: null,
        createdAt: null,
        updatedAt: null
    }
}

export default function login(state = initState, action){
    switch(action.type){
        case LOGIN:
            return {...state, loginInfo: action.params};
        default:
            return state;
    }
}