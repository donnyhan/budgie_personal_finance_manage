import React,{ useCallback, useEffect, useRef, useState } from 'react';
import { Card, Row, Col, Statistic, Alert, message, Table, Select, Button, Space, Input, Divider, DatePicker, Empty, Modal } from 'antd';
import Skeleton from '@yisheng90/react-loading';
import PropTypes from 'prop-types';
import './index.scss';
import {isDummyMode, logging} from 'utils/config'
import { useHistory } from 'react-router';
import { delCookie, formatMoney, handleGlobalErrorFunc, unformatMoney, useHasChanged, useWindowSize } from 'utils';
import { chartDataDummy } from 'dummy/dashboard/chartDataDummy';
import _ from 'lodash';
import { cardDataDummy } from 'dummy/dashboard/cardDataDummy';
import { tableDataDummy } from 'dummy/dashboard/tableDataDummy';
import Pagination from 'rc-pagination';
import { DeleteOutlined, EditOutlined, ExclamationCircleOutlined, PlusOutlined, PlusSquareOutlined, SearchOutlined } from '@ant-design/icons'; 
import { useDispatch, useSelector } from 'react-redux';
import { accountsDataDummy } from '../../dummy/dashboard/accountsDataDummy';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment';
import {getAccountsListAPI, deleteAccountAPI, getTransactionsListAPI, deleteTransactionsAPI} from 'api/finance'
import AccountsModal from './accountsModal';
import TransactionsModal from './transactionsModal';
import {LiabilitiesIcon, AssetsIcon, AccountsIcon} from "utils/icons";

const Option = Select.Option;
const { Search } = Input;

const Finance = (props) => {
    const { RangePicker } = DatePicker;
    const history = useHistory()
    const dispatch = useDispatch();
    const loginState  = useSelector(state => state.login)
    const loginInfo = _.get(loginState, 'loginInfo', {})
    const initTableSchema = {
        list: [],
        currentPage: 1,
        pageSize: -1,
        totalItem: 0
    }
    const [hasData, setHasData] = useState(false);
    const [queryAccounts, setQueryAccounts] = useState(null)
    const size = useWindowSize();
    const [tableData, setTableData] = useState(initTableSchema);
    const [tableLoadingState, setTableLoadingState] = useState(false)
    const [accountsList, setAccountsList] = useState([]);
    const [rangePickerValue, setRangePickerValue] = useState([moment().startOf('month'), moment().endOf('month')]);
    const [isCreate, setIsCreate] = useState(false)
    const [accountModalIsShown, setAccountModalIsShown] = useState(false);
    const [transactionModalIsShown, setTransactionModalIsShown] = useState(false);
    const [currentRecordAccount, setCurrentRecordAccount] = useState(null);
    const [currentRecordTransactions, setCurrentRecordTransactions] = useState(null);
    useEffect(() => {
        const unblock = history.block((location, action) => {
            if (location.pathname === '/login' && action === 'POP') {
              return false;
            }
            return true;
        });
        getAccountsData();
        return () => {
            unblock();
        }
    }, [])
    
    const getAccountsData = async () => {
        let params = {};
        let res = {};
        if (isDummyMode()) {
            res = accountsDataDummy();
            setAccountsList(res.data)
        } else {
            try {
                res = await getAccountsListAPI(params);
                setAccountsList(res.data);
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }
    };

    const getTableData = async (page, pageSize) => {
        let params = {};
        params.page = page || tableData.currentPage;
        params.pageSize = pageSize || tableData.pageSize;
        params.from = moment(rangePickerValue[0]).toISOString();
        params.to = moment(rangePickerValue[1]).toISOString();
        params.query = queryAccounts;
        logging('getTableData params', params)
        let res = {};
       if ((isDummyMode())) {
            res = tableDataDummy();
            let tmpTable = {...res.data, pageSize: initTableSchema.pageSize, page: initTableSchema.currentPage};
            setTableData(tmpTable)
        } else {
            try {
                res = await getTransactionsListAPI(params);
                setTableData(res.data);
                setHasData(true)
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }
    };
    
    const tableDataColumn = [
        {
            title: 'Transaction',
            width: 400,
            align: 'left',
            dataIndex: 'date',
            key: 'date',
            render(txt, record, idx) {
                return <div key={idx}>
                            <Row className='bold-text'>
                                {record.name}
                            </Row>
                            <Row className="grey-text">
                                {moment(record.date).format('DD MMM YYYY HH:mm:ss')}
                            </Row>
                        </div>
            }
        },
        {
            title: 'Amount',
            width: 150,
            align: 'left',
            dataIndex: 'amount',
            key: 'amount',
            render(txt, record, idx) {
                return <Space key={idx}>
                            <div className='bold-text'>
                                {formatMoney(record.amount)}
                            </div>
                        </Space>
            }
        },
        {
            title: 'Action',
            width: 50,
            align: 'left',
            render(txt, record, idx) {
                return <Space key={idx}>
                            <Button type='default' onClick={() => editTransaction(record)} icon={<EditOutlined />}>
                            </Button>
                            <Button type='default' danger onClick={() => deleteTransaction(record)} icon={<DeleteOutlined />}>
                            </Button>
                        </Space>
            },
        },
    ]

    const addTransaction = () => {
        setCurrentRecordTransactions(null);
        setIsCreate(true);
        setTransactionModalIsShown(true);
    }

    const deleteTransaction = (record) => {
        Modal.confirm({
            title: 'Are you sure want to delete?',
            icon: <ExclamationCircleOutlined />,
            content: (
                  <div className='confirm-modal-content'>
                      Are you sure want to delete?
                  </div>
            ),
            okText: 'Yes, Delete',
            cancelText: 'Cancel',
            onOk: async () => {
                let params = {
                    id: record.id
                };
                let res = {};
                try {
                    res = await deleteTransactionsAPI(params);
                    getTableData();
                } catch (error) {
                    handleGlobalErrorFunc(error)
                }
            }
          });
    }

    const editTransaction = (record) => {
        setCurrentRecordTransactions(record);
        setIsCreate(false);
        setTransactionModalIsShown(true);
    }

    const addAccount = () => {
        setCurrentRecordAccount(null);
        setIsCreate(true);
        setAccountModalIsShown(true);
    }

    const editAccount = (record) => {
        setCurrentRecordAccount(record);
        setIsCreate(false);
        setAccountModalIsShown(true);
    }

    const deleteAccount = (record) => {
        Modal.confirm({
            title: 'Are you sure want to delete?, all transactions belong to this account will deleted.',
            icon: <ExclamationCircleOutlined />,
            content: (
                  <div className='confirm-modal-content'>
                      Are you sure want to delete?, all transactions belong to this account will deleted.
                  </div>
            ),
            okText: 'Yes, Delete',
            cancelText: 'Cancel',
            onOk: async () => {
                let params = {
                    id: record.id
                };
                let res = {};
                try {
                    res = await deleteAccountAPI(params);
                    getAccountsData();
                    getTableData();
                } catch (error) {
                    handleGlobalErrorFunc(error)
                }
            }
          });
    }

    useEffect(() => {
        getTableData(1, tableData.pageSize);
    }, [rangePickerValue, queryAccounts])

    const onRangePickerChanged = (dates, dateStrings) => {
        setRangePickerValue(dates)
    }

    const onSearch = (value) => {
        setQueryAccounts(value);
    };

    const renderTable = () => {
        return (
            <>
                <Table
                    rowKey={(record, idx) => {
                        return record.uuidClient
                    }}
                    showHeader={false}
                    loading={tableLoadingState}
                    bordered
                    columns={tableDataColumn}
                    dataSource={tableData.list}
                    style={{ minWidth: '960px' }}
                    locale={
                    {
                        emptyText: 'No data'
                    }}
                    pagination={false}
                    bordered={false}
                />
            </>
        )
    }

    
    return (
        <>
        {
            (hasData) ?
                <div className='finance'>
                    <Row>
                        <Col xs={24} sm={24} md={18} lg={18} xl={18} className="left-box">
                            <Card title="" className="card-wrapper" bordered={false}>
                                <Row className="margin-vertical-10">
                                    <Col xs={24} sm={24} md={19} lg={14} xl={14}>
                                        <h3>Financial Transaction</h3>
                                    </Col>
                                    <Col xs={24} sm={24} md={5} lg={5} xl={5}>
                                        <Button type="dashed" icon={<PlusOutlined/>} onClick={() => addTransaction()}>
                                            <span className="grey-text">Add Transaction</span>
                                        </Button>
                                    </Col>
                                    <Col xs={24} sm={24} md={5} lg={5} xl={5}>
                                        <Search placeholder="Search Transactions by Name on current period" allowClear onSearch={onSearch} enterButton />
                                    </Col>
                                </Row>
                                <Row>
                                    <RangePicker inputReadOnly={true} allowClear={false} size={'default'} defaultValue={rangePickerValue} format={'DD MMM YYYY'} onChange={onRangePickerChanged}/>
                                </Row>
                                <Row>
                                    <Scrollbars className="scrollbar-finance-transaction margin-vertical-10" style={{height: (size && size.height) ? `${size.height - 260}px` : '300px'}}>
                                        {renderTable()}
                                    </Scrollbars>
                                </Row>
                            </Card>
                        </Col>
                        <Col xs={24} sm={24} md={5} lg={5} xl={5} className="right-box">
                            <Card title="" bordered={false} className="card-wrapper">
                                <Row className="margin-vertical-10">
                                    <Col span={19}>
                                        <h3>Accounts</h3>
                                    </Col>
                                    <Col span={5}>
                                        <Button type="dashed" onClick={() => addAccount()} icon={<PlusOutlined className="grey-dashed-icon"/>}></Button>
                                    </Col>
                                </Row>
                                <div className="accounts-list-wrapper">
                                    <Scrollbars className="scrollbar-account-list" style={{height: (size && size.height) ? `${size.height - 210}px` : '300px'}}>
                                    {
                                        (accountsList && accountsList.list && accountsList.list.length > 0) ?
                                            (accountsList.list).map((item, idx) => {
                                                return (
                                                    <div key={idx} className="stats-card">
                                                        <Card className="stat-card" bordered={false}>
                                                            <Row>
                                                                <Col span={6}>
                                                                    <div className="margin-vertical-10">
                                                                        <AccountsIcon style={{ fontSize: '32px' }} />
                                                                    </div>
                                                                </Col>
                                                                <Col span={14}>
                                                                    <div className="label margin-top-10">
                                                                        <h4 className="overflow-ellipsis">{item.label}</h4>
                                                                    </div>
                                                                </Col>
                                                                <Col span={2}>
                                                                    <Button type='default' onClick={() => editAccount(item)} icon={<EditOutlined />}></Button>
                                                                    <Button type="dashed" onClick={() => deleteAccount(item)} icon={<DeleteOutlined/>} danger></Button>
                                                                </Col>
                                                            </Row>
                                                        </Card>  
                                                    </div>
                                                )
                                            })
                                        :
                                            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                                    }
                                    </Scrollbars>
                                </div>
                            </Card>
                        </Col>
                    </Row>
                </div>
            :
            <div className='finance'>
                <Row>
                    <Col xs={24} sm={24} md={18} lg={18} xl={18}>
                        <Skeleton height={`${size.height / 2}px`} width="100%"/>
                    </Col>
                    <Col xs={24} sm={24} md={5} lg={5} xl={5} className="right-box">
                        <Skeleton height={`${size.height / 2}px`} width="100%"/>
                    </Col>
                </Row>
            </div>
        }
        {
            accountModalIsShown &&
                <AccountsModal 
                    onCancel={() => {
                        setAccountModalIsShown(false)
                        setIsCreate(false);
                        setCurrentRecordAccount(null);
                    }}
                    onOk={() => {
                        setAccountModalIsShown(false)
                        setIsCreate(false);
                        setCurrentRecordAccount(null);
                        getAccountsData();
                    }}
                    isCreate={isCreate}
                    currentRecord={currentRecordAccount}
                ></AccountsModal>
        }
        {
            transactionModalIsShown &&
                <TransactionsModal 
                    onCancel={() => {
                        setTransactionModalIsShown(false)
                        setIsCreate(false);
                        setCurrentRecordTransactions(null);
                    }}
                    onOk={() => {
                        setTransactionModalIsShown(false)
                        setIsCreate(false);
                        setCurrentRecordTransactions(null);
                        getTableData();
                    }}
                    isCreate={isCreate}
                    currentRecord={currentRecordTransactions}
                ></TransactionsModal>
        }
        </>
    )
}
Finance.propTypes = {
    type: PropTypes.any
}
export default Finance;