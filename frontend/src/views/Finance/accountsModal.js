import React, {useState, useEffect} from 'react';
import { Form, Input, message, Modal } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons'
import PropTypes from 'prop-types';
import { useHistory, userHistory } from  'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { handleGlobalErrorFunc } from 'utils';
import _ from 'lodash';
import { createAccountsAPI, editAccountsAPI } from 'api/finance';
import { logging } from 'utils/config';
const Item = Form.Item;
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const AccountsModal = (props) => {
    const {onCancel, onOk, isCreate, currentRecord} = props;
    const [form] = Form.useForm();
    const history = useHistory();
    const dispatch = useDispatch();
    const loginState  = useSelector(state => state.login)
    const loginInfo = _.get(loginState, 'loginInfo', {})
    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 }
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 }
        }
    };

    useEffect(() => {
        form.setFieldsValue({
            name: (isCreate) ? null : currentRecord.label
        })
    }, [])

    const onOkFunc = () => {
        form.validateFields().then( async (values)=>{
            let params = {
                name: values.name
            }
            if (!isCreate) {
                params.id = currentRecord.id
                try {
                    const res = await editAccountsAPI(params);
                    onOk()
                } catch (error) {
                    handleGlobalErrorFunc(error)
                }
            } else {
                try {
                    const res = await createAccountsAPI(params);
                    onOk()
                } catch (error) {
                    handleGlobalErrorFunc(error)
                }
                
            }
            
        }).catch((err)=>{
            logging('err', err)
        })
    }

    return (
        <Modal
            title="Accounts"
            visible={true}
            okText="OK"
            cancelText="Cancel"
            onOk={onOkFunc}
            onCancel={onCancel}
            width={'70%'}
            closable={false}
        >
            <Form {...layout} form={form} name="accounts">
                <Item
                    {...formItemLayout}
                    name="name"
                    label="Label"
                    rules={
                        [
                            {
                                required: true, message: "Label is required"
                            }
                        ]
                    }
                >
                    <Input placeholder="Enter Label"></Input>
                </Item>
            </Form>
        </Modal>
    )
}
AccountsModal.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onOk:PropTypes.func.isRequired
}
export default AccountsModal;