import React, {useState, useEffect} from 'react';
import { Form, Input, message, Modal, Select, DatePicker } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons'
import PropTypes from 'prop-types';
import { useHistory, userHistory } from  'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { currencyMask, handleGlobalErrorFunc, unformatMoney, formatMoney } from 'utils';
import _ from 'lodash';
import { createTransactionsAPI, editTransactionsAPI, getAccountsListAPI } from 'api/finance';
import { isDummyMode, logging } from 'utils/config';
import moment from 'moment';
import MaskedInput from 'react-text-mask';
import { accountsDataDummy } from 'dummy/dashboard/accountsDataDummy';
const Item = Form.Item;
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const Option = Select.Option;
const TransactionsModal = (props) => {
    const {onCancel, onOk, isCreate, currentRecord} = props;
    const [form] = Form.useForm();
    const history = useHistory();
    const dispatch = useDispatch();
    const loginState  = useSelector(state => state.login)
    const [accountsList, setAccountsList] = useState([]);
    const loginInfo = _.get(loginState, 'loginInfo', {})
    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 }
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 }
        }
    };

    useEffect(() => {
        getAccountsData();
        form.setFieldsValue({
            name: (isCreate) ? null : currentRecord.name,
            amount: (isCreate) ? null : formatMoney(currentRecord.amount),
            date: (isCreate) ? null : moment(currentRecord.date),
            accountId: (isCreate) ? null : currentRecord.accountId
        })
    }, [])

    const getAccountsData = async () => {
        let params = {};
        let res = {};
        if (isDummyMode()) {
            res = accountsDataDummy();
            setAccountsList(res.data)
        } else {
            try {
                res = await getAccountsListAPI(params);
                setAccountsList(res.data);
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }
    };

    const onOkFunc = () => {
        form.validateFields().then( async (values)=>{
            let params = {
                name: values.name,
                accountId: values.accountId,
                date: values.date.toISOString(),
                amount: unformatMoney(values.amount)
            }
            if (!isCreate) {
                params.id = currentRecord.id
                try {
                    const res = await editTransactionsAPI(params);
                    onOk()
                } catch (error) {
                    handleGlobalErrorFunc(error)
                }
            } else {
                try {
                    const res = await createTransactionsAPI(params);
                    onOk()
                } catch (error) {
                    handleGlobalErrorFunc(error)
                }
                
            }
            
        }).catch((err)=>{
            logging('err', err)
        })
    }
    const renderAccountsDropdown = () => {
        let listSource = (accountsList && accountsList.list) ? accountsList.list : [];
        let ops = []
        for(let idx in listSource){
            if (listSource[idx]['id']) {
                ops.push(
                    <Option key={idx} value={listSource[idx]['id']}>{listSource[idx]['label']}</Option>
                )
            }
        }
        return ops;
    }
    const onChangeAccount = (e) => {
        form.setFieldsValue({
            accountId: e
        })
    }

    const onSelectDatePicker = (e) => {
        let date = moment(e);
        form.setFieldsValue({
            date: date
        })
    }
    return (
        <Modal
            title="Transactions"
            visible={true}
            okText="OK"
            cancelText="Cancel"
            onOk={onOkFunc}
            onCancel={onCancel}
            width={'70%'}
            closable={false}
        >
            <Form {...layout} form={form} name="transaction">
                <Item
                    {...formItemLayout}
                    name="name"
                    label="Name"
                    rules={
                        [
                            {
                                required: true, message: "Name is required"
                            }
                        ]
                    }
                >
                    <Input placeholder="Enter Name"></Input>
                </Item>
                <Item
                    {...formItemLayout}
                    name="accountId"
                    label="Account"
                    rules={
                        [
                            {
                                required: true, message: "Account is required"
                            }
                        ]
                    }
                >
                    <div>
                        <Select 
                            dropdownStyle = {{ position: "fixed" }}
                            placeholder='Choose Account'
                            showSearch
                            optionFilterProp="children"
                            onChange={(e) => onChangeAccount(e)}
                            filterOption={(input, option) =>
                                option && option.children && option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            defaultValue={_.get(currentRecord, 'accountId', null)}
                            >
                            {
                                renderAccountsDropdown()
                            }
                        </Select>
                    </div>
                </Item>
                <Item
                    {...formItemLayout}
                    name="date"
                    label="Date"
                    rules={
                        [
                            {
                                required: true, message: "Date is required"
                            }
                        ]
                    }
                >
                    <DatePicker
                        format="DD MMM YYYY HH:mm"
                        showTime 
                        onSelect={(e) => {
                            onSelectDatePicker(e)
                        }}/>
                </Item>
                <Item
                    {...formItemLayout}
                    name="amount"
                    label="Amount"
                    rules={
                        [
                            {
                                required: true, message: "Amount is required"
                            }
                        ]
                    }
                    tooltip={'* Use Minus Sign (-) as prefix if you would like to consider as expense'}
                    extra={'* Use Minus Sign (-) as prefix if you would like to consider as expense'}
                >
                    <div>
                        <MaskedInput
                            className="input-number" 
                            value={(currentRecord && currentRecord.amount) ? currentRecord.amount : null}
                            defaultValue={(currentRecord && currentRecord.amount) ? currentRecord.amount : null}
                            mask={currencyMask} 
                            inputMode="numeric"
                            placeholder='Please enter amount'
                            onChange={(e) => {
                                form.setFieldsValue({
                                    amount: e
                                })
                            }}
                            onFocus={(e) => {
                                if (e.target.value === '0') {
                                    e.target.value = ''
                                }
                            }}
                            onBlur={(e) => {
                                if (e.target.value === '') {
                                    e.target.value = '0'
                                }
                            }}
                            />
                    </div>
                </Item>
            </Form>
        </Modal>
    )
}
TransactionsModal.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onOk:PropTypes.func.isRequired
}
export default TransactionsModal;