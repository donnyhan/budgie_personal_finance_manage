import React, { useEffect, useState } from 'react';
import {  Layout } from 'antd';
import './index.scss'
import HeaderComponent from 'components/HeaderComponent';
import RouteMain from '../Route';
import _ from 'lodash';
const { Content } = Layout;
    
const LayoutComponent = () => {
    return(
        <Layout className='layoutMain'>
            <HeaderComponent></HeaderComponent>
            <Layout>
                <Layout className='layoutContent'>
                    <Content>
                        <RouteMain></RouteMain>
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    )
}
export default  LayoutComponent;