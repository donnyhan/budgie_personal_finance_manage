import React,{ useCallback, useEffect, useState } from 'react';
import { Card, Row, Col, Statistic, Alert, message, Table, Select, Button, Space, Input, Divider, DatePicker, Empty } from 'antd';
import Skeleton from '@yisheng90/react-loading';
import PropTypes from 'prop-types';
import './index.scss';
import {isDummyMode, logging, lineChartConfig} from 'utils/config'
import { useHistory } from 'react-router';
import { delCookie, formatMoney, formatNumberWithComma, handleGlobalErrorFunc, unformatMoney, useWindowSize } from 'utils';
import { chartDataDummy } from 'dummy/dashboard/chartDataDummy';
import _ from 'lodash';
import { cardDataDummy } from 'dummy/dashboard/cardDataDummy';
import Pagination from 'rc-pagination';
import { SearchOutlined } from '@ant-design/icons'; 
import { useDispatch, useSelector } from 'react-redux';
import { Line } from "@ant-design/charts";
import {LiabilitiesIcon, AssetsIcon} from "utils/icons";
import moment from 'moment';
import { getTopExpensesAPI, getTransactionsChartDataAPI } from 'api/finance';
const Option = Select.Option;
const Home = (props) => {
    const { RangePicker } = DatePicker;
    const history = useHistory()
    const dispatch = useDispatch();
    const menu = useSelector(state => state.menu)
    const loginState  = useSelector(state => state.login)
    const [hasChartData, setHasChartData] = useState(false);
    const [hasCardData, setHasCardData] = useState(false);
    const size = useWindowSize();
    const [cardStatBodyWidth, setCardStatBodyWidth] = useState('90%')
    const [cardData, setCardData] = useState([]);
    const [chartData, setChartData] = useState([]);
    const [rangePickerValue, setRangePickerValue] = useState([moment().startOf('month'), moment().endOf('month')]);
    const [totalAmountLeft, setTotalAmountLeft] = useState(0);
    useEffect(() => {
        const unblock = history.block((location, action) => {
            if (location.pathname === '/login' && action === 'POP') {
              return false;
            }
            return true;
        });
        getChartData();
        getCardData();
        return () => {
            unblock();
        }
    }, [rangePickerValue])

    useEffect(() => {
        const tmpCardStatBodyWidth = (size.width) ? (size.width / 5) : '50%'
        setCardStatBodyWidth(tmpCardStatBodyWidth)
    }, [size])

    const [statsData, setStatsData] = useState([
        {label: ''},
        {label: ''},
        {label: ''},
        {label: ''},
        {label: ''},
    ])

    const cardStatTitleWidth = "50%"
    
    const getCardData = async () => {
        let params = {
            from: moment(rangePickerValue[0]).toISOString(),
            to: moment(rangePickerValue[1]).toISOString()
        };
        let res = {};
        if (isDummyMode()) {
            res = cardDataDummy();
            setCardData(res.data)
            setHasCardData(true)
        } else {
            try {
                res = await getTopExpensesAPI(params);
                setCardData(res.data);
                setHasCardData(true)
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }
    };

    const getChartData = async () => {
        let params = {
            from: rangePickerValue[0].toISOString(),
            to: rangePickerValue[1].toISOString()
        };
        let res = {};
        let amountLeft = 0;
        if (isDummyMode()) {
            res = chartDataDummy();
            let resData = res.data;
            let formattedData = (resData).map((item, idx) => {
                amountLeft += Number(item.sum);
                item.formattedSum = formatMoney((item.sum) ? item.sum : 0);
                item.date = moment(item.date).format('DD MMM YYYY');
                return item;
            })
            setChartData(formattedData)
            setTotalAmountLeft(amountLeft)
            setHasChartData(true)
        } else {
            try {
                res = await getTransactionsChartDataAPI(params);
                let resData = res.data;
                let formattedData = (resData).map((item, idx) => {
                    amountLeft += Number(item.sum);
                    item.formattedSum = formatMoney((item.sum) ? item.sum : 0);
                    item.date = moment(item.date).format('DD MMM YYYY');
                    return item;
                })
                setChartData(formattedData)
                setTotalAmountLeft(amountLeft)
                setHasChartData(true)
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }
    }

    const onRangePickerChanged = (dates, dateStrings) => {
        setRangePickerValue(dates)
    }

    let chartConfig = lineChartConfig(chartData);
    
    return (
        <>
        {
            (hasCardData || hasChartData) ?
                <div className='home'>
                    <Row className="date-wrapper">
                        <Col span={24}>
                            <Card title="" style={{ width: '100%' }} bordered={false}>
                                <div className="margin-vertical-10">
                                    <Row>
                                        <Col xs={24} sm={24} md={19} lg={19} xl={19}>
                                            <RangePicker inputReadOnly={true} allowClear={false} size={'default'} defaultValue={rangePickerValue} format={'DD MMM YYYY'} onChange={onRangePickerChanged}/>
                                        </Col>
                                        <Col xs={24} sm={24} md={5} lg={5} xl={5}>
                                            <span className="bold-text">{`Your Balance on Current Period: ${formatMoney(totalAmountLeft)}`}</span>
                                        </Col>
                                    </Row>
                                </div>
                                <Line {...chartConfig} />
                            </Card>                    
                        </Col>
                    </Row>
                    <div className="card-wrapper">
                        <div className="margin-vertical-10 margin-horizontal-10">
                            <h3>Biggest Expense on Current Period</h3>
                        </div>
                        <div className="card-area">
                            {
                                (cardData && cardData.length > 0) ?
                                    (cardData).map((item, idx) => {
                                        return (
                                            <div key={idx} className="stats-card">
                                                <Card className="stat-card" bordered={false} style={{maxWidth: cardStatBodyWidth}}>
                                                    <div className="margin-vertical-10">
                                                        <LiabilitiesIcon style={{ fontSize: '32px' }} />
                                                    </div>
                                                    <div className="label">
                                                        <div>
                                                            <h4>{item.name}</h4>
                                                        </div>
                                                        <div className="grey-text">
                                                            {moment(item.date).format('DD MMM YYYY HH:mm')}
                                                        </div>
                                                    </div>
                                                    <div className="amount">
                                                        {formatMoney(item.amount)}
                                                    </div>
                                                </Card>  
                                            </div>
                                        )
                                    })
                                :
                                <div className="empty-wrapper">
                                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                                </div>
                            }
                        </div>
                    </div>
                </div>
            :
            <div className='home'>
                {/* {size.width}px / {size.height}px */}
                <Row className="chart-wrapper">
                    <Col span={24}>
                        <Card title="" style={{ width: '100%' }} bordered={false}>
                            <div className="margin-vertical-10">
                                <Skeleton height="1.5rem" width="20%"/>
                            </div>
                            <Skeleton height="20rem" width="100%"/>
                        </Card>                    
                    </Col>
                </Row>
                <div className="card-wrapper">
                    {
                        (statsData).map((item, idx) => {
                            return (
                                <div key={idx} className="stats-card">
                                    <Card className="stat-card" bordered={false} style={{maxWidth: cardStatBodyWidth}}>
                                        <div className="margin-vertical-10">
                                            <Skeleton height="1.5rem" width={cardStatTitleWidth}/>
                                        </div>
                                        <Skeleton height="3rem" width={cardStatBodyWidth}/>
                                    </Card>  
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        }
        </>
    )
}
Home.propTypes = {
    type: PropTypes.any
}
export default Home;