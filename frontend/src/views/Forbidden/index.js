import React from 'react'
import { Button, Result } from 'antd'
import { useHistory } from 'react-router'
import './index.scss'
import { useDispatch } from 'react-redux'
const Forbidden = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const backToIndex = () => {
        history.push('/index/home')
    }
    return (
        <Result
            status="403"
            title="403"
            subTitle="Maaf, anda tidak memiliki otorisasi untuk mengakses halaman ini."
            extra={<Button type="primary" onClick={backToIndex}>Kembali ke Halaman Awal</Button>}
        />
    )
}
export default Forbidden;