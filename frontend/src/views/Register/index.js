import React, { useCallback, useEffect, useState } from 'react'
import { Input, Form, Button, message, Select, Checkbox, Row, Col, Layout } from 'antd'
import {
    UserOutlined,
    LockOutlined,
    EyeTwoTone,
    EyeInvisibleOutlined
} from '@ant-design/icons';
import {
    login,
    getCurrentProfile
} from 'api/login'
import loadable from '@loadable/component';
import { useHistory } from 'react-router-dom'
import './index.scss';
import { enCodePassword, logging, isDummyMode } from 'utils/config';
import { delCookie, getCookie, handleGlobalErrorFunc, setCookie, setParentChildOnMenu } from 'utils/index'
import { useDispatch, useSelector } from 'react-redux';
import { setLoginInfo } from 'action/login';
import _ from 'lodash';

const FormItem = Form.Item;
const { Option } = Select;
const formItemLayout = {
    labelCol: { span: 0 },
    wrapperCol: { span: 24 },
};
const tailLayout = {
    wrapperCol: { offset: 0, span: 12 },
  };
  
const Register = () =>{
    const [form] = Form.useForm();
    const history = useHistory();
    const dispatch = useDispatch();
    const [temporaryLoginData, setTemporaryLoginData] = useState(null);
    const {loginInfo}  = useSelector(state => state.login)
    
    useEffect(() => {
        const unblock = history.block((location, action) => {
            if (location.pathname === '/index/home' && action === 'POP') {
              return false;
            }
            return true;
        });
        return () => {
            unblock();
        }
    }, [])

    
    const registrationHandle = ()=>{
        form.validateFields().then(async (values)=>{
            const params = {
                email: values.email,
                password: values.password
            }
            let res = {};
            try {
                res = await login(params);
                redirectAndProcessTemporaryLoginData(res);
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }).catch((e)=>{
            logging(e)
        })
    }

    const backToLogin = () => {
        history.go(-2)
    }
    
    const redirectAndProcessTemporaryLoginData = (dt) => {
        logging('dt', dt)
        let currentToken = _.get(dt, 'token', null);
        dispatch(setLoginInfo({
            token: currentToken, 
            name: null,
            email: null,
            id: null
        }));
        setTemporaryLoginData(null);
        /* get profile detail */
        getProfileDetail(currentToken);
    }

    const getProfileDetail = async (currentToken) => {
        let res = {};
        let userId = null;
        try {
            res = await getCurrentProfile();
            userId = _.get(res, 'user.id', null)
            dispatch(setLoginInfo({
                token: currentToken, 
                email: _.get(res, 'user.email', null),
                id: userId
            }));
        } catch (error) {
            handleGlobalErrorFunc(error)
        }
    }

    return (
        <div className='login-main'>
            <div className='login-bg'>
                <div className='login-box align-left'>
                    <div className="hello-text">
                        <h3 className="hello">Create new account at Budgie</h3>
                    </div>
                    <div className="error-message">
                    </div>
                    <Form form={form} {...formItemLayout} onFinish={registrationHandle}>
                        <FormItem
                            name='email'
                            rules={
                                [
                                    {
                                        required: true,
                                        message: 'E-mail is required'
                                    }
                                ]
                            }
                        >
                            <Input placeholder='Please enter E-mail address' />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            name='password'
                            rules={
                                [
                                    {
                                        required: true,
                                        message: 'Password is required'
                                    }
                                ]
                            }
                        >
                            <Input.Password placeholder='Please enter password' iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}/>
                        </FormItem>
                        <FormItem {...formItemLayout}>
                            <Button type='primary' className='login-form-button btn-warning' onClick={registrationHandle} htmlType="submit">
                            Sign me up
                            </Button>
                        </FormItem>
                        <FormItem {...formItemLayout}>
                            <Button type='link' className='registration-link-button' onClick={backToLogin} htmlType="button">
                                Back to login
                            </Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default Register;