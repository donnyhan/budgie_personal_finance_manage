import React, { useCallback, useEffect, useState } from 'react'
import { Input, Form, Button, message, Select, Checkbox, Row, Col, Layout } from 'antd'
import {
    UserOutlined,
    LockOutlined,
    EyeTwoTone,
    EyeInvisibleOutlined
} from '@ant-design/icons';
import {
    login,
    getCurrentProfile
} from 'api/login'
import loadable from '@loadable/component';
import { useHistory } from 'react-router-dom'
import './index.scss';
import { enCodePassword, logging, isDummyMode } from 'utils/config';
import { delCookie, getCookie, handleGlobalErrorFunc, setCookie, setParentChildOnMenu } from 'utils/index'
import { useDispatch, useSelector } from 'react-redux';
import { setLoginInfo } from 'action/login';
import * as moment from 'moment';
import _ from 'lodash';

const FormItem = Form.Item;
const { Option } = Select;
const formItemLayout = {
    labelCol: { span: 0 },
    wrapperCol: { span: 24 },
};
const tailLayout = {
    wrapperCol: { offset: 0, span: 12 },
  };
  
const Login = () =>{
    const [form] = Form.useForm();
    const history = useHistory();
    const dispatch = useDispatch();
    const [temporaryLoginData, setTemporaryLoginData] = useState(null);
    const {loginInfo}  = useSelector(state => state.login)
    
    useEffect(() => {
        const unblock = history.block((location, action) => {
            if (location.pathname === '/index/home' && action === 'POP') {
              return false;
            }
            return true;
        });
        return () => {
            unblock();
        }
    }, [])

    
    const loginHandle = ()=>{
        form.validateFields().then(async (values)=>{
            const params = {
                email: values.email,
                password: values.password
            }
            let res = {};
            try {
                res = await login(params);
                redirectAndProcessTemporaryLoginData(res);
            } catch (error) {
                handleGlobalErrorFunc(error)
            }
        }).catch((e)=>{
            logging(e)
        })
    }

    const goToRegistration = () => {
        history.push('/register')
    }
    
    const redirectAndProcessTemporaryLoginData = (dt) => {
        logging('dt', dt)
        let currentToken = _.get(dt, 'token', null);
        let user = _.get(dt, 'user', {});
        dispatch(setLoginInfo({
            token: currentToken, 
            email: user.email,
            id: user.id,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt
        }));
        setTemporaryLoginData(null);
        /* get profile detail */
        getProfileDetail(currentToken);
    }

    const getProfileDetail = async (currentToken) => {
        let res = {};
        try {
            res = await getCurrentProfile();
            console.log('res', res)
            let user = _.get(res, 'user', {});
            dispatch(setLoginInfo({
                token: currentToken, 
                email: user.email,
                id: user.id,
                createdAt: user.createdAt,
                updatedAt: user.updatedAt
            }));
        } catch (error) {
            handleGlobalErrorFunc(error)
        }
    }

    return (
        <div className='login-main'>
            <div className='login-bg'>
                <div className='login-box align-left'>
                    <div className="hello-text">
                        <h3 className="hello">Welcome to Budgie</h3>
                    </div>
                    <div className="error-message">
                    </div>
                    <Form form={form} {...formItemLayout} onFinish={loginHandle}>
                        <FormItem
                            name='email'
                            rules={
                                [
                                    {
                                        required: true,
                                        message: 'E-mail Diperlukan'
                                    }
                                ]
                            }
                        >
                            <Input placeholder='Please enter E-mail address' />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            name='password'
                            rules={
                                [
                                    {
                                        required: true,
                                        message: 'Password is required'
                                    }
                                ]
                            }
                        >
                            <Input.Password placeholder='Please enter password' iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}/>
                        </FormItem>
                        <FormItem {...formItemLayout}>
                            <Button type='primary' className='login-form-button btn-warning' onClick={loginHandle} htmlType="submit">
                            Login
                            </Button>
                        </FormItem>
                        <FormItem {...formItemLayout}>
                            <Button type='link' className='registration-link-button' onClick={goToRegistration} htmlType="button">
                                Create an account
                            </Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default Login;