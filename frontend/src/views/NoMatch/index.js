import React from 'react'
import { Button } from 'antd'
import { useHistory } from 'react-router'
import './index.scss'
const NoMatch = () => {
    const history = useHistory();
    const backToIndex = () => {
        history.push('/index/home')
    }
    return (
        <div>
            <div className='main-404'>
                <div className='main-img'>
                    <div className='bg-img'></div>
                </div>
                <div className='txt'>
                    <h1>404</h1>
                    <div className='des'>Halaman tidak ditemukan</div>
                    <Button type='primary' onClick={backToIndex}>Kembali</Button>
                </div>
            </div>
        </div>
    )
}
export default NoMatch;