import React, { useEffect } from 'react'

import { Redirect, Route, Switch, useHistory, useLocation } from 'react-router-dom'
import Home from '../Home';
import Finance from '../Finance';
import NoMatch from '../NoMatch'
import { getObjects, PrivateRoute, useHasChanged } from 'utils';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import Forbidden from 'views/Forbidden';

const RouteMain = () => {
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    return (
        <Switch>
            <PrivateRoute exact path='/index/home' component={Home}></PrivateRoute>
            <PrivateRoute exact path='/index/finance' component={Finance}></PrivateRoute>
            <PrivateRoute exact path='/index/this-will-no-match-menu' component={NoMatch}></PrivateRoute>
            <PrivateRoute exact path='/index/unauthorized' component={Forbidden}></PrivateRoute>
            <Route path='/index/*'>
                <NoMatch></NoMatch>
            </Route>
        </Switch>
    )
}

export default RouteMain;