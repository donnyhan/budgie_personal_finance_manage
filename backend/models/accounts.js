'use strict';
module.exports = (sequelize, DataTypes) => {
  var Accounts = sequelize.define('Accounts', {
    name: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  Accounts.associate = function(models) {
    // associations can be defined here
  };
  
  Accounts.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());
    return values;
  };
  return Accounts;
};