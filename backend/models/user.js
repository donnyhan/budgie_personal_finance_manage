'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  
  User.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());
  
    delete values.password;
    delete values.token;
  
    return values;
  };
  return User;
};