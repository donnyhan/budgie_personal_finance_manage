'use strict';
module.exports = (sequelize, DataTypes) => {
  var Transactions = sequelize.define('Transactions', {
    date: DataTypes.STRING,
    amount: DataTypes.DECIMAL,
    name: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    accountId: DataTypes.INTEGER
  }, {});
  Transactions.associate = function(models) {
    // associations can be defined here
  };
  
  Transactions.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());
    return values;
  };
  return Transactions;
};