"use strict";

const timestamp = new Date();
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Transactions",
      [
        {
          name: "Shopping Cost",
          userId: 1,
          accountId: 1,
          createdAt: timestamp,
          updatedAt: timestamp,
          date: timestamp,
          amount: -200000
        },
        {
          name: "Clothes & Shoes",
          userId: 1,
          accountId: 1,
          createdAt: timestamp,
          updatedAt: timestamp,
          date: timestamp,
          amount: -200000
        },
        {
          name: "Current Cash",
          userId: 1,
          accountId: 1,
          createdAt: timestamp,
          updatedAt: timestamp,
          date: timestamp,
          amount: 1000000
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Transactions", null, {});
  },
};
