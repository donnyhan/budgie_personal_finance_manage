"use strict";

const timestamp = new Date();
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Accounts",
      [
        {
          name: "Cash",
          userId: 1,
          createdAt: timestamp,
          updatedAt: timestamp
        },
        {
          name: "Card",
          userId: 1,
          createdAt: timestamp,
          updatedAt: timestamp
        },
        {
          name: "Savings",
          userId: 1,
          createdAt: timestamp,
          updatedAt: timestamp
        },
        {
          name: "Top Up / Prepaid",
          userId: 1,
          createdAt: timestamp,
          updatedAt: timestamp
        },
        {
          name: "Loan",
          userId: 1,
          createdAt: timestamp,
          updatedAt: timestamp
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Accounts", null, {});
  },
};
