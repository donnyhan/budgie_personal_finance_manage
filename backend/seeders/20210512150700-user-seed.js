"use strict";

const timestamp = new Date();
const bcryptService = require('../services/bcrypt.service');
module.exports = {
  up: (queryInterface, Sequelize) => {
    let stringPass = "password"
    let hash = bcryptService().generateHash(stringPass)
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          email: "user1@mail.com",
          password: hash,
          createdAt: timestamp,
          updatedAt: timestamp,
          token: null
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
