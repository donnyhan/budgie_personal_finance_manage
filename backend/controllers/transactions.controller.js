const Transactions = require("../models").Transactions;
const User = require("../models").User;
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const db = require('../models');
const moment = require('moment');

// @desc   transactions
// @route  POST /transactions/list
exports.transactionsList = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { page, pageSize, query, from, to } = req.body;
    if (auth && from && to) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Bad Request: User not found' });
                } else {
                    let fromDate = moment(from).toDate()
                    let toDate = moment(to).toDate()
                    let transactionsList = await db.sequelize.query('SELECT id, name, date, amount, accountId FROM Transactions WHERE userId = :userId AND date between :fromDate AND :toDate AND name LIKE :query ORDER BY updatedAt DESC', {
                        replacements: {
                            userId: user.id,
                            fromDate: fromDate,
                            toDate: toDate,
                            query: (query) ? `%${query}%` : "%%"
                        },
                        type: db.sequelize.QueryTypes.SELECT
                    });
                    return res.status(200).json({
                        data: { list : (transactionsList) ? transactionsList : []}
                    });
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   transactions
// @route  POST /transactions/topExpenses
exports.transactionsTopExpenses = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { from, to } = req.body;
    if (auth && from && to) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Bad Request: User not found' });
                } else {
                    let fromDate = moment(from).toDate()
                    let toDate = moment(to).toDate()
                    let transactionsList = await db.sequelize.query('SELECT id, name, amount, date FROM Transactions WHERE amount < 0 AND date between :fromDate AND :toDate ORDER BY amount ASC LIMIT 5', {
                        replacements: {
                            fromDate: fromDate,
                            toDate: toDate,
                        },
                        type: db.sequelize.QueryTypes.SELECT
                    });
                    return res.status(200).json({
                        data: (transactionsList) ? transactionsList : []
                    });
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   transactions
// @route  POST /transactions/transactionsChartData
exports.transactionsChartData = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { from, to } = req.body;
    if (auth && from && to) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Bad Request: User not found' });
                } else {
                    let fromDate = moment(from).toDate()
                    let toDate = moment(to).toDate()
                    let transactionsList = await db.sequelize.query('SELECT SUM (amount) as sum, category, date FROM (SELECT IF(amount < 0, "Expense", "Income") as category, date, amount FROM Transactions WHERE date between :fromDate AND :toDate ORDER BY date ASC) AS res GROUP BY DATE(date), category ', {
                        replacements: {
                            fromDate: fromDate,
                            toDate: toDate,
                        },
                        type: db.sequelize.QueryTypes.SELECT
                    });
                    return res.status(200).json({
                        data: (transactionsList) ? transactionsList : []
                    });
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   transactions
// @route  POST /transactions/create
exports.transactionsCreate = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { name, accountId, date, amount  } = req.body;
    if (auth && name && accountId && date && amount) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Token Expired' });
                } else {
                    let timestamp = new Date();
                    let dateParam = moment(date).toDate();
                    await db.sequelize.query('INSERT INTO Transactions (name, userId, accountId, date, amount, createdAt, updatedAt) VALUES (:name, :userId, :accountId, :date, :amount, :createdAt, :updatedAt)', {
                        replacements: {
                            name: name,
                            userId: user.id,
                            accountId: accountId,
                            date: dateParam,
                            amount: amount,
                            createdAt: timestamp,
                            updatedAt: timestamp,
                        },
                        type: db.sequelize.QueryTypes.INSERT
                    });
                    return res.status(200).json({});
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   transactions
// @route  POST /transactions/edit
exports.transactionsEdit = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { id, name, accountId, date, amount } = req.body;
    if (auth && name && accountId && date && amount) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Token Expired' });
                } else {
                    let dateParam = moment(date).toDate();
                    let timestamp = new Date();
                    await db.sequelize.query('UPDATE Transactions SET name = :name, updatedAt = :updatedAt, amount = :amount, date = :date where id = :recordId', {
                        replacements: {
                            recordId: id,
                            name: name,
                            updatedAt: timestamp,
                            date: dateParam,
                            amount: amount,
                            accountId: accountId
                        },
                        type: db.sequelize.QueryTypes.UPDATE
                    });
                    return res.status(200).json({});
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   transactions
// @route  POST /transactions/delete
exports.transactionsDelete = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { id } = req.body;
    if (auth && id) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Token Expired' });
                } else {
                    let timestamp = new Date();
                    await db.sequelize.query('DELETE FROM Transactions where id = :recordId', {
                        replacements: {
                            recordId: id
                        },
                        type: db.sequelize.QueryTypes.DELETE
                    });
                    return res.status(200).json({});
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}