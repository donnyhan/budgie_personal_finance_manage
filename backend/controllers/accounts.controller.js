const Accounts = require("../models").Accounts;
const User = require("../models").User;
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const db = require('../models');

// @desc   accounts
// @route  POST /accounts/list
exports.accountsList = async (req, res, next) => {
    const auth = req.headers.authorization;
    if (auth) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Bad Request: User not found' });
                } else {
                    let accountsList = await db.sequelize.query('SELECT id, name as label FROM Accounts WHERE userId = :userId ORDER BY updatedAt DESC', {
                        replacements: {userId: user.id},
                        type: db.sequelize.QueryTypes.SELECT
                    });
                    return res.status(200).json({
                        data: { list : (accountsList) ? accountsList : []}
                    });
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   accounts
// @route  POST /accounts/create
exports.accountsCreate = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { name } = req.body;
    if (auth && name) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Token Expired' });
                } else {
                    let timestamp = new Date();
                    let accountsList = await db.sequelize.query('INSERT INTO Accounts (name, userId, createdAt, updatedAt) VALUES (:name, :userId, :createdAt, :updatedAt)', {
                        replacements: {
                            name: name,
                            userId: user.id,
                            createdAt: timestamp,
                            updatedAt: timestamp,
                        },
                        type: db.sequelize.QueryTypes.INSERT
                    });
                    return res.status(200).json({});
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth && !name) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   accounts
// @route  POST /accounts/edit
exports.accountsEdit = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { name, id } = req.body;
    if (auth && name && id) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Token Expired' });
                } else {
                    let timestamp = new Date();
                    await db.sequelize.query('UPDATE Accounts SET name = :name, updatedAt = :updatedAt where id = :recordId', {
                        replacements: {
                            recordId: id,
                            name: name,
                            updatedAt: timestamp
                        },
                        type: db.sequelize.QueryTypes.UPDATE
                    });
                    return res.status(200).json({});
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth && (!name || !id)) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}

// @desc   accounts
// @route  POST /accounts/delete
exports.accountsDelete = async (req, res, next) => {
    const auth = req.headers.authorization;
    const { id } = req.body;
    if (auth && id) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                const user = await User
                .findOne({
                    where: {
                        token,
                    },
                });

                if (!user) {
                    return res.status(401).json({ message: 'Token Expired' });
                } else {
                    let timestamp = new Date();
                    await db.sequelize.query('DELETE a.*, b.* FROM Accounts a LEFT JOIN Transactions b ON b.userId = a.userId WHERE a.id = :recordId AND b.accountId = :recordId AND a.userId = :userId', {
                        replacements: {
                            recordId: id,
                            userId: user.id
                        },
                        type: db.sequelize.QueryTypes.DELETE
                    });
                    return res.status(200).json({});
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth && !id) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}