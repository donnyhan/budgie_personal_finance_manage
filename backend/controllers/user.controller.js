const User = require("../models").User;
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
const db = require('../models');
// @desc   login
// @route  POST /user/login
const doAfterSuccessLogin = async (res, token, user) => {
    await db.sequelize.query('UPDATE Users set token = :token WHERE id = :id', {
        replacements: {id: user.id, token: token},
        type: db.sequelize.QueryTypes.UPDATE
    });
    // res.header('Access-Control-Expose-Headers', 'Authorization')
    // res.setHeader('Authorization', `Bearer ${token}`); 
    return res.status(200).json({ user: (user && user.dataValues) ? {
        id: (user.dataValues.id) ? user.dataValues.id : null,
        email: (user.dataValues.email) ? user.dataValues.email : null,
        createdAt: (user.dataValues.createdAt) ? user.dataValues.createdAt : null,
        updatedAt: (user.dataValues.updatedAt) ? user.dataValues.updatedAt : null,
    } : null, token });
}
exports.login = async (req, res, next) => {
    const { email, password } = req.body;
    if (email && password) {
        try {
            const user = await User
                .findOne({
                    where: {
                        email,
                    },
                });

            if (!user) {
                return res.status(400).json({ message: 'Bad Request: User not found' });
            }

            if (bcryptService().comparePassword(password, user.password)) {
                const token = authService().issue({ id: user.id });
                doAfterSuccessLogin(res, token, user)
            } else {
                return res.status(400).json({ message: 'Email or password is wrong' });
            }
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Bad Request: Email or password is wrong' });
        }
    } else {
        return res.status(400).json({ message: 'Bad Request: Email or password is needed' });
    }
}

// @desc   register
// @route  POST /user/register
exports.register = async (req, res, next) => {
    const { email, password } = req.body;
    if (email && password) {
        try {
            let hash = bcryptService().generateHash(password)
            const user = await User.create({
                email: email,
                password: hash,
            });
            // const token = authService().issue({ id: user.id });

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    }

    return res.status(400).json({ message: 'Bad Request: Email or password is wrong' });
}

// @desc   profile
// @route  GET /user/profile
exports.profile = async (req, res, next) => {
    const auth = req.headers.authorization;
    if (auth) {
        try {
            const token = auth.split(' ')[1];
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                let user = await db.sequelize.query('SELECT email, id, createdAt, updatedAt FROM Users WHERE token = :token LIMIT 1', {
                    replacements: {token: token},
                    type: db.sequelize.QueryTypes.SELECT
                });
                return res.status(200).json({ user: (user && user.length > 0) ? user[0] : null });
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}
// @desc   changePassword
// @route  POST /user/changePassword
exports.changePassword = async (req, res, next) => {
    const { oldPassword, newPassword } = req.body;
    const auth = req.headers.authorization;
    if (auth && oldPassword && newPassword) {
        try {
            const token = auth.split(' ')[1];
            let newPasswordHash = bcryptService().generateHash(newPassword)
            authService().verify(token, async (err) => {
                if (err) {
                  return res.status(401).json({ message: 'Token Expired' });
                }
                let user = await db.sequelize.query('SELECT email, id, createdAt, updatedAt, password FROM Users WHERE token = :token LIMIT 1', {
                    replacements: {token: token},
                    type: db.sequelize.QueryTypes.SELECT
                });
                if (user && user.length > 0) {
                    if (bcryptService().comparePassword(oldPassword, user[0]['password'])) {
                        await db.sequelize.query('UPDATE Users set password = :newPasswordHash WHERE id = :id', {
                            replacements: {id: user[0]['id'], newPasswordHash: newPasswordHash},
                            type: db.sequelize.QueryTypes.UPDATE
                        });
                        return res.status(200).json({});
                    } else {
                        return res.status(400).json({ message: 'Email or password is wrong' });
                    }
                } else {
                    return res.status(401).json({ message: 'Token Expired' });
                }
            });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ message: 'Internal server error' });
        }
    } else if (auth && (!oldPassword || !newPassword)) {
        return res.status(400).json({ message: 'Bad Request' });
    } else {
        return res.status(401).json({ message: 'Unauthorized' });
    }
}