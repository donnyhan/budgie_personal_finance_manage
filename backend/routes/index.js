const express =  require('express');
const router = express.Router();
const { login, register, profile, changePassword } = require('../controllers/user.controller');
const { accountsList, accountsCreate, accountsEdit, accountsDelete } = require('../controllers/accounts.controller');
const { transactionsList, transactionsCreate, transactionsEdit, transactionsDelete, transactionsTopExpenses, transactionsChartData } = require('../controllers/transactions.controller');

router.post('/user/login', login);
router.post('/user/register', register)
router.post('/user/changePassword', changePassword)
router.get('/user/profile', profile)

router.post('/accounts/list', accountsList)
router.post('/accounts/create', accountsCreate)
router.post('/accounts/edit', accountsEdit)
router.post('/accounts/delete', accountsDelete)

router.post('/transactions/list', transactionsList)
router.post('/transactions/create', transactionsCreate)
router.post('/transactions/edit', transactionsEdit)
router.post('/transactions/delete', transactionsDelete)

router.post('/transactions/topExpenses', transactionsTopExpenses)
router.post('/transactions/chartData', transactionsChartData)

module.exports = router;