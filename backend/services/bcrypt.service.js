const bcrypt = require('bcrypt');

const bcryptService = () => {
  const generateHash = (string) => {
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(string, salt);
    return hash;
  }
  const password = (user) => {
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(user.password, salt);

    return hash;
  };

  const comparePassword = (pw, hash) => (
    bcrypt.compareSync(pw, hash)
  );

  return {
    password,
    comparePassword,
    generateHash
  };
};

module.exports = bcryptService;