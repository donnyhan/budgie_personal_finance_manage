# Budgie - simple personal finance manager

Menjalankan Budgie web app ada 2 cara:

## Cara #1: Melalui run project backend dan frontend secara manual
> Pastikan node.js dan yarn sudah terinstall di local vm. 

### Pada 2 terminal terpisah:

 #### terminal pertama (project backend):
``
npm install -g sequelize-cli
``
``
npm install -g sequelize
``
``
npm install -g mysql2
``
``
npm install -g nodemon
``
> lalu pastikan di root directory project backend untuk install dependencies project
``
npm install
``
> konfigurasi di .env.run agar sesuai dengan kebutuhan di vm.
``
cp -r .env.run .env
``
> jika sudah, lakukan migrate dan seed terlebih dahulu:
``
sequelize-cli db:create && sequelize-cli db:migrate && sequelize-cli db:seed:all && nodemon index.js
``
> jika gagal, bisa drop database existing atau undo-all:
``
sequelize-cli db:seed:undo:all
``
> dan apabila sukses, jalankan backend app dengan perintah sebagai berikut:
``
npm run start
``
> selanjutnya backend app bisa diakses dari port 8081. untuk ip bisa menyesuaikan vm masing-masing.

#### terminal kedua (project frontend):
> konfigurasi di .env.run agar sesuai dengan kebutuhan.
``
cp -r .env.run .env
``
>pastikan yarn terinstall di vm
``
npm install -g yarn
``
>install project dependencies
``
yarn install
``
> jalankan project frontend
``
yarn run start
``
> selanjutnya frontend app bisa diakses dari port 3000. untuk ip bisa menyesuaikan vm masing-masing.

## Cara #2: Menjalankan melalui docker
> pastikan docker sudah terinstall di vm dan support specs versi 3.7
> di project backend pastikan konfigurasi .env.docker sudah sesuai di vm.
``
cp -r .env.docker .env
``
> di project frontend pastikan konfigurasi .env.docker sudah sesuai di vm, Dockerfile menggunakan build command yg langsung menggunakan .env.docker.
> kembali ke root directory project utama
``
docker-compose build
``
> jika image sudah terbentuk, jalankan up dengan detached mode
``
docker-compose up -d
``
> jika ingin mematikan instance container
``
docker-compose down
``
> akses frontend dari port 80, dan backend akan dari port 8080 dan dapat diakses melalui 127.0.0.1 / localhost.

## # user seed default
Untuk user yg dapat digunakan user1@mail.com:password atau juga bisa melalui halaman register / API via postman.

Terima kasih